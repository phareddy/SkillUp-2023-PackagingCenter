package com.skillup.apigatewayserver;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.support.ServerWebExchangeUtils;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.server.ServerWebExchange;

import reactor.core.publisher.Mono;

@Component
public class AuthFilter implements GlobalFilter, Ordered {

	private static Logger logger = LogManager.getLogger(AuthFilter.class);
	
	private static final String ORDER_SERVICE_KEY = "cGhhbmk6cGhhbmk=";
	
	@Override
	public int getOrder() {
		return Ordered.LOWEST_PRECEDENCE;
	}

	@Override
	public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
		String reqPath = exchange.getRequest().getPath().pathWithinApplication().value();
		logger.error("Request URL {}", reqPath);
		
		if(reqPath != null && (reqPath.contains("swagger") || reqPath.contains("api-docs"))) {
			return chain.filter(exchange);
		}
						
		List<String> apiKeyHeader = exchange.getRequest().getHeaders().get("gatewayKey");
		logger.info("Api key: {}", apiKeyHeader);

        Route route = exchange.getAttribute(ServerWebExchangeUtils.GATEWAY_ROUTE_ATTR);
         
        String routeId = route != null ? route.getId() : null;
        if (routeId == null || CollectionUtils.isEmpty(apiKeyHeader) || !isAuthorized(routeId, apiKeyHeader.get(0))) {
        	logger.error("Api Key not valid");
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "You cannot consume this service. Please check your api key.");
        }

        return chain.filter(exchange);
	}
	
	private boolean isAuthorized(String routeId, String apikey) {
		return ORDER_SERVICE_KEY.equals(apikey);
    }
}
