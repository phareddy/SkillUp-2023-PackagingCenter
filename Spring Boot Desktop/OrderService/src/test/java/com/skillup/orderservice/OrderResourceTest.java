package com.skillup.orderservice;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;

import java.net.URI;
import java.util.Arrays;
import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.skillup.orderservice.controller.OrderResource;
import com.skillup.orderservice.entity.Address;
import com.skillup.orderservice.entity.Item;
import com.skillup.orderservice.entity.ItemAddress;
import com.skillup.orderservice.entity.Order;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class OrderResourceTest {

	private static WireMockServer wireMockServer = null;

	@MockBean	
	private LoadBalancerClient loadBalancerClient;
	
	@Autowired
	private OrderResource orderResource;
	
	@BeforeAll
	public static void setUp() {
		wireMockServer = new WireMockServer(new WireMockConfiguration().dynamicPort());
		wireMockServer.start();		
	}
	
	@BeforeEach
	public void beforeEach() {
		MockitoAnnotations.openMocks(this);
	}
	
	@Test
	public void testPlaceOrder() {		
				
		Mockito.when(loadBalancerClient.choose("PACKAGING-SERVICE")).thenReturn(getServiceInstance(wireMockServer.baseUrl()));		
	    wireMockServer.stubFor(WireMock.post("/packageservice/SortPackage").willReturn(aResponse().withBody("OK").withStatus(200)));

	    ItemAddress itemAddress1 = ItemAddress.builder().address(Address.builder().line1("Line1").line2("Line2").city("Chennai").state("Tamil Nadu").build()).item(Item.builder().id(100).name("TV").price(100.00).quantity(1).total(100.00).build()).build();
		ItemAddress itemAddress2 = ItemAddress.builder().address(Address.builder().line1("Line1").line2("Line2").city("Chennai").state("Tamil Nadu").build()).item(Item.builder().id(100).name("RADIO").price(100.00).quantity(1).total(100.00).build()).build();
		ItemAddress itemAddress3 = ItemAddress.builder().address(Address.builder().line1("Line1").line2("Line2").city("Chennai").state("Punjab").build()).item(Item.builder().id(100).name("TV").price(100.00).quantity(1).total(100.00).build()).build();
		Order order = Order.builder().customerId(100).itemAddresses(Arrays.asList(itemAddress1,itemAddress2,itemAddress3)).build();
	    
	    orderResource.placeOrder(order);
	}
	
	private ServiceInstance getServiceInstance(String url) {
		
		ServiceInstance serviceInstance = new ServiceInstance() {
			
			@Override
			public boolean isSecure() {
				return false;
			}
			
			@Override
			public URI getUri() {
				return URI.create(url);
			}
			
			@Override
			public String getServiceId() {
				return null;
			}
			
			@Override
			public int getPort() {
				return 0;
			}
			
			@Override
			public Map<String, String> getMetadata() {
				return null;
			}
			
			@Override
			public String getHost() {
				return null;
			}
		};
		return serviceInstance;
	}
	
}
