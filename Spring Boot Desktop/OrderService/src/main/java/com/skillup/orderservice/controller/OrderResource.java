package com.skillup.orderservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.skillup.orderservice.entity.Address;
import com.skillup.orderservice.entity.ItemAddress;
import com.skillup.orderservice.entity.Order;
import com.skillup.orderservice.service.OrderService;
import com.skillup.orderservice.utils.OrderServiceUtils;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping(path = "/orderservice")
public class OrderResource {
	
	@Autowired
	private OrderService orderService;
	
	@Operation(description = "Place an order for a list of Item Address")
	@PostMapping(value = "/PlaceOrder")
	public String placeOrder(@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Order to be processed") 
							@RequestBody Order order) {
		List<ItemAddress> itemAddresses = order.getItemAddresses();
		for (ItemAddress itemAddress : itemAddresses) {
			Address address = itemAddress.getAddress();
			if(!OrderServiceUtils.isValidState(address.getState())) {
				return "Invalid Statecode " + address.getState();
			}
		}
		return orderService.sortPackage(itemAddresses);
	}

	
}
