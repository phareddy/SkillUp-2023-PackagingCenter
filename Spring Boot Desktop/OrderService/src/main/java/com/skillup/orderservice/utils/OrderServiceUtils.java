package com.skillup.orderservice.utils;

import java.util.Arrays;
import java.util.List;

public class OrderServiceUtils {

	private static List<String> stateList = Arrays.asList("Himachal Pradesh","Punjab","Uttarakhand","Uttar Pradesh","Haryana",
			"Madhya Pradesh","Chhattisgarh","Bihar","Orissa","Jharkhand","West Bengal","Assam","Sikkim",
			"Nagaland","Meghalaya","Manipur","Mizoram","Tripura","Arunachal Pradesh","Rajasthan","Gujarat","Goa",
			"Maharashtra","Andhra Pradesh","Karnataka","Kerala","Tamil Nadu","Telangana");

	public static boolean isValidState(String state) {
		return stateList.contains(state);
	}
	
}
