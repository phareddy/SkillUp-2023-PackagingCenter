package com.skillup.orderservice.service;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.skillup.orderservice.entity.ItemAddress;

import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.RetryRegistry;
import io.github.resilience4j.retry.annotation.Retry;

@Service
public class OrderService {

	@Autowired
	private LoadBalancerClient loadBalancerClient;
	
	private static Logger logger = LogManager.getLogger(OrderService.class);
	
	@Autowired
    private RetryRegistry retryRegistry;

	@Autowired
	private CircuitBreakerRegistry circuitBreakerRegistry;
		
	@PostConstruct
    public void postConstruct() {
        retryRegistry.retry("psRetryConfig").getEventPublisher()
                .onRetry(ev -> logger.info("#### RetryRegistryEventListener message: {}", ev));
        
        circuitBreakerRegistry.circuitBreaker("psCircuitConfig").getEventPublisher()
        		.onEvent(ev -> logger.info("#### CircuitBreakerRegistryEventListener message: {}", ev));
        
    }
	
	@CircuitBreaker(name = "psCircuitConfig", fallbackMethod = "circuitOpen")
	@Retry(name = "psRetryConfig", fallbackMethod = "defaultSortPackage")	
	public String sortPackage(List<ItemAddress> itemAddresses) {
		logger.info("Start callPackageService");
		ServiceInstance serviceInstance = loadBalancerClient.choose("PACKAGING-SERVICE");
		if(serviceInstance == null) {
			throw new RuntimeException("PACKAGING-SERVICE NOT FOUND");
		}
		logger.info("Connecting to PACKAGING-SERVICE Instance {}", serviceInstance.getUri()); 
		RestTemplate restTemplate = new RestTemplate();		
		ResponseEntity<String> re = restTemplate.postForEntity(serviceInstance.getUri() + "/packageservice/SortPackage", itemAddresses, String.class);
		return re.getBody();
	}
	
	public String defaultSortPackage(List<ItemAddress> itemAddresses, Throwable throwable) {
		return "Packaging Service is down";
	}
	
	public String circuitOpen(List<ItemAddress> itemAddresses, Throwable throwable) {
		return "Packaging Service circuit open";
	}
	
}
