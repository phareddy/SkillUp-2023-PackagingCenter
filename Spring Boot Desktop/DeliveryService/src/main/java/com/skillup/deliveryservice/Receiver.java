package com.skillup.deliveryservice;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.skillup.packageservice.entity.Package;

@Component
public class Receiver {

	private static Logger logger = LogManager.getLogger(Receiver.class);

	private ObjectMapper objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
	
	public void receiveMessage(String message) {		
		try {
			Package packageObj = objectMapper.readValue(message, Package.class);		
			String msg = objectMapper.writeValueAsString(packageObj);
			logger.info("Package Request Received For Zone {}, Message {}", packageObj.getStateZone(), msg);
		} catch (JsonProcessingException e) {
			logger.error("Exception when parsing message", e);
		}
		
	}

}