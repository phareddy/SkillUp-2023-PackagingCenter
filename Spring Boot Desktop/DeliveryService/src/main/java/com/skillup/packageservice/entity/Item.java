package com.skillup.packageservice.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @NoArgsConstructor @ToString  @AllArgsConstructor @Builder
public class Item {

	private long id;
	private String name;
	private long quantity;
	private double price;
	private double total;
}
