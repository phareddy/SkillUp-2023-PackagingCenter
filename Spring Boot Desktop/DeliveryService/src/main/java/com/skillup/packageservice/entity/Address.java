package com.skillup.packageservice.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @NoArgsConstructor @EqualsAndHashCode @ToString @AllArgsConstructor @Builder
public class Address {
	
	private String line1;
	private String line2;
	private String city;
	private String state;
	
}
