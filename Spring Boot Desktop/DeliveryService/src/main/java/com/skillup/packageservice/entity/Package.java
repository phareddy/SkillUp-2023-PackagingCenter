package com.skillup.packageservice.entity;

import java.util.List;

import com.skillup.packageservice.util.StateZone;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @NoArgsConstructor @ToString  @AllArgsConstructor @Builder
public class Package {
	
	private StateZone stateZone;
	private String packageId;
	private Address address;
	private List<Item> invoice;
	
}
