package com.skillup.packageservice.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.skillup.packageservice.entity.Address;
import com.skillup.packageservice.entity.Item;
import com.skillup.packageservice.entity.ItemAddress;
import com.skillup.packageservice.entity.Package;
import com.skillup.packageservice.util.PackageServiceUtils;

@RestController
@RequestMapping(path = "/packageservice")
public class PackageResource {

	private final RabbitTemplate rabbitTemplate;
	
	private static final String queueName = "test-queue";
	
	public PackageResource(RabbitTemplate rabbitTemplate) {
		this.rabbitTemplate = rabbitTemplate;
	}

	@PostMapping(value = "/SortPackage")
	public ResponseEntity<String> sortPackage(@RequestBody List<ItemAddress> itemAddresses) {
		List<Package> packages = new ArrayList<>();
		
		Map<Address, List<Item>> itemsByAddress = new HashMap<>();
		
		for (ItemAddress itemAddress : itemAddresses) {
			if(!itemsByAddress.containsKey(itemAddress.getAddress())) {
				itemsByAddress.put(itemAddress.getAddress(), new ArrayList<>());
			}
			itemsByAddress.get(itemAddress.getAddress()).add(itemAddress.getItem());
		}
		
		for (Map.Entry<Address, List<Item>> entry : itemsByAddress.entrySet()) {
			Package package1 = new Package();
			Address address = entry.getKey();
			List<Item> items = entry.getValue();
			
			package1.setPackageId(UUID.randomUUID().toString());
			package1.setStateZone(PackageServiceUtils.getStateZone(address.getState()));
			package1.setAddress(address);
			package1.setInvoice(items);
			
			packages.add(package1);
		}
		
		Gson gson = new Gson();
		for (Package package1 : packages) {
			String msg = gson.toJson(package1);
			rabbitTemplate.convertAndSend(queueName, msg);
		}
		
		return ResponseEntity.ok("OK");
	}

	
}
