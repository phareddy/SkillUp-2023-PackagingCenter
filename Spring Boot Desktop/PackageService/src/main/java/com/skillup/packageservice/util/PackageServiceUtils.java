package com.skillup.packageservice.util;

import java.util.Arrays;
import java.util.List;

public class PackageServiceUtils {
	
	private static List<String> northZoneList = Arrays.asList("Himachal Pradesh","Punjab","Uttarakhand","Uttar Pradesh","Haryana","Madhya Pradesh","Chhattisgarh");
	
	private static List<String> eastZoneList = Arrays.asList("Bihar","Orissa","Jharkhand","West Bengal","Assam","Sikkim","Nagaland","Meghalaya","Manipur","Mizoram","Tripura","Arunachal Pradesh");
	
	private static List<String> westZoneList = Arrays.asList("Rajasthan","Gujarat","Goa","Maharashtra");
	
	private static List<String> southZoneList = Arrays.asList("Andhra Pradesh","Karnataka","Kerala","Tamil Nadu");
		
	public static StateZone getStateZone(String state) {
		if(northZoneList.contains(state)) {
			return StateZone.NORTH;
		} else if(eastZoneList.contains(state)) {
			return StateZone.EAST;
		} else if(westZoneList.contains(state)) {
			return StateZone.WEST;
		} else if(southZoneList.contains(state)) {
			return StateZone.SOUTH;
		} else {
			return StateZone.NONE;
		}
	}
	
}
