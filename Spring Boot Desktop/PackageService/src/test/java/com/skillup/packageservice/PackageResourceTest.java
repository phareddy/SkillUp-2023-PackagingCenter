package com.skillup.packageservice;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.skillup.packageservice.controller.PackageResource;
import com.skillup.packageservice.entity.Address;
import com.skillup.packageservice.entity.Item;
import com.skillup.packageservice.entity.ItemAddress;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PackageResourceTest {

	private static WireMockServer wireMockServer = null;

	@MockBean	
	private RabbitTemplate rabbitTemplate;
	
	@Autowired
	private PackageResource packageResource;
	
	@BeforeAll
	public static void setUp() {
		wireMockServer = new WireMockServer(new WireMockConfiguration().dynamicPort());
		wireMockServer.start();		
	}
	
	@BeforeEach
	public void beforeEach() {
		MockitoAnnotations.openMocks(this);
	}
	
	@Test
	public void testPlaceOrder() {		
		Mockito.doNothing().when(rabbitTemplate).convertAndSend(Mockito.anyString(), Mockito.anyString());				
	    wireMockServer.stubFor(WireMock.post("/packageservice/SortPackage").willReturn(aResponse().withBody("OK").withStatus(200)));
	    ItemAddress itemAddress1 = ItemAddress.builder().address(Address.builder().line1("Line1").line2("Line2").city("Chennai").state("Tamil Nadu").build()).item(Item.builder().id(100).name("TV").price(100.00).quantity(1).total(100.00).build()).build();
		ItemAddress itemAddress2 = ItemAddress.builder().address(Address.builder().line1("Line1").line2("Line2").city("Chennai").state("Tamil Nadu").build()).item(Item.builder().id(100).name("RADIO").price(100.00).quantity(1).total(100.00).build()).build();
		ItemAddress itemAddress3 = ItemAddress.builder().address(Address.builder().line1("Line1").line2("Line2").city("Chennai").state("Punjab").build()).item(Item.builder().id(100).name("TV").price(100.00).quantity(1).total(100.00).build()).build();
		List<ItemAddress> itemAddresses = Arrays.asList(itemAddress1,itemAddress2,itemAddress3);	    
		packageResource.sortPackage(itemAddresses);
	}
		
}
